using Microsoft.AspNetCore.Components;

namespace PhotinoBlazorTailwind.Desktop.Pages;

public partial class Index : ComponentBase
{
    private string? TextContent { get; set; }

    private void OnClick_ThrowButton()
    {
        throw new Exception();
    }

    private async void OnClick_OpenFile()
    {
        using var dialog = new OpenFileDialog();

        dialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);

        var result = dialog.ShowDialog();

        switch (result)
        {
            case DialogResult.OK:
                using (var stream = dialog.OpenFile())
                using (var reader = new StreamReader(stream))
                {
                    TextContent = await reader.ReadToEndAsync();
                    StateHasChanged();
                }
                break;
        }
    }
}
