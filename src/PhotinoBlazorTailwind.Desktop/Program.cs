using Photino.Blazor;

using PhotinoBlazorTailwind.Desktop;

class Program
{

    [STAThread]
    static void Main(string[] args)
    {
        var builder = PhotinoBlazorAppBuilder.CreateDefault(args);

        builder.RootComponents.Add<App>("#app");

        var app = builder.Build();

        app.MainWindow.ContextMenuEnabled = true;

        app.Run();
    }
}
