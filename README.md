# Photino Blazor Tailwind Sample
Photino.Blazorを使用し、Blazor WASMテンプレートからデスクトップアプリを作成する。また、UIはTailwindによってスタイリングする。

## 手順

### 新規プロジェクト作成

```
mkdir photino-blazor-tailwind
cd photino-blazor-tailwind

dotnet new sln --name PhotinoBlazorTailwind
dotnet new blazorwasm -o src/PhotinoBlazorTailwind.Desktop
dotnet sln add src/PhotinoBlazorTailwind.Desktop
```

### プロジェクト設定変更

#### `PhotinoBlazorTailwind.Desktop.csproj`
1. SDK変更 (`BlazorWebAssembly` -> `Razor`)
2. 出力形式を `Exe` に変更
3. WebAssembly関連パッケージを削除
4. Photino.Blazorパッケージを追加

```diff
@@ -1,14 +1,20 @@
-<Project Sdk="Microsoft.NET.Sdk.BlazorWebAssembly">
+<Project Sdk="Microsoft.NET.Sdk.Razor">

   <PropertyGroup>
     <TargetFramework>net6.0</TargetFramework>
     <Nullable>enable</Nullable>
     <ImplicitUsings>enable</ImplicitUsings>
+    <OutputType>Exe</OutputType>
   </PropertyGroup>

   <ItemGroup>
-    <PackageReference Include="Microsoft.AspNetCore.Components.WebAssembly" Version="6.0.24" />
-    <PackageReference Include="Microsoft.AspNetCore.Components.WebAssembly.DevServer" Version="6.0.24" PrivateAssets="all" />
+    <PackageReference Include="Photino.Blazor" Version="2.6.0" />
+  </ItemGroup>
+
+  <ItemGroup>
+    <Content Update="wwwroot/**">
+      <CopyToOutputDirectory>PreserveNewest</CopyToOutputDirectory>
+    </Content>
   </ItemGroup>

 </Project>
```

#### `_Imports.razor`
以下のような不要になった行を削除。
```diff
@@ -4,7 +4,6 @@
 @using Microsoft.AspNetCore.Components.Routing
 @using Microsoft.AspNetCore.Components.Web
 @using Microsoft.AspNetCore.Components.Web.Virtualization
-@using Microsoft.AspNetCore.Components.WebAssembly.Http
 @using Microsoft.JSInterop
 @using PhotinoBlazorTailwind.Desktop
 @using PhotinoBlazorTailwind.Desktop.Shared
```

#### `wwwroot/index.html`
`webassembly.js` から `webview.js` に変更

```diff
@@ -19,7 +19,7 @@
         <a href="" class="reload">Reload</a>
         <a class="dismiss">🗙</a>
     </div>
-    <script src="_framework/blazor.webassembly.js"></script>
+    <script src="_framework/blazor.webview.js"></script>
 </body>

 </html>
```

#### `Program.cs`
丸ごと置き換え。
```cs
using Photino.Blazor;

using PhotinoBlazorTailwind.Desktop;

class Program
{

    [STAThread]
    static void Main(string[] args)
    {
        var builder = PhotinoBlazorAppBuilder.CreateDefault(args);

        builder.RootComponents.Add<App>("#app");

        var app = builder.Build();

        app.Run();
    }
}
```

## Quick Tips

### WinFormsのファイルダイアログを使う

- TargetFramework: net6.0-windows
- UseWindowsForms: true

```diff
 <Project Sdk="Microsoft.NET.Sdk.Razor">

   <PropertyGroup>
-    <TargetFramework>net6.0</TargetFramework>
+    <TargetFramework>net6.0-windows</TargetFramework>
     <Nullable>enable</Nullable>
     <ImplicitUsings>enable</ImplicitUsings>
     <OutputType>WinExe</OutputType>
+    <UseWindowsForms>true</UseWindowsForms>
   </PropertyGroup>

   <ItemGroup>
```
